import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(9125)) {
            System.out.println("Ожидание клиента");

            try (Socket socket = serverSocket.accept()) {
                System.out.println("Клиент " + socket.getInetAddress() + " подключен");
                try (InputStream inputStream = socket.getInputStream();
                     OutputStream outputStream = socket.getOutputStream();
                     DataInputStream input = new DataInputStream(inputStream);
                     DataOutputStream output = new DataOutputStream(outputStream)) {

                    while (true) {
                        output.writeUTF(input.readUTF());
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("*** ServerSocket ERROR *** \n " + e);
        }
    }
}
