import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {
        try (Socket clientSocket = new Socket("localhost", 9125)){
            try(InputStream inputStream = clientSocket.getInputStream();
                OutputStream outputStream = clientSocket.getOutputStream();
                DataInputStream input = new DataInputStream(inputStream);
                DataOutputStream output = new DataOutputStream(outputStream)){

                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                String text;
                System.out.println("Введите что-нибудь.");

                while(true){
                    System.out.println("Ожидание ввода...");
                    text = reader.readLine();
                    if (text.equals("client.exit")){
                        System.out.println("Отключение от сервера");
                        break;
                    }
                    System.out.println("Отправка на сервер");
                    output.writeUTF(text);
                    System.out.println("Ответ сервера: " + input.readUTF());
                }
            }
        }catch (IOException e){
            System.out.println("*** Socket ERROR *** \n" + e);
        }

}
}
